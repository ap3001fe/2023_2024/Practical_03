# Practical 03
Jupyter notebook explaining the concept of assembly in finite elements in 1D.

Partially shows for high order finite elements (assembly not implemented yet), but fully implemented for piecewise linear basis.

## Getting started

Just download it and run the notebook. You only need numpy and scipy, which you should have already.

## License
Creative commons: CC BY-NC.
